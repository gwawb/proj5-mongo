Adam Christensen 
christe2@uoregon.edu

## ACP controle times calculator

That's "controle" with an 'e', because it's French, although "control" is also accepted. The race is called a brevet. A brevet consists of controls which are checkpoints where a bike rider must obtain proof of passage in oreder to continue in the brevet. This calculator determines the minimum and maximum control[e] times by which the rider must arrive at the location.

The times are calculated by a set of rules determined by Randonneurs USA. The formula is as follows:

The closing and opening times of a controle are determined by the time it would take a rider to reach the controle from the beginning of the brevet going the minimum speed and the maximum speed respectively. The minimum and maximum speeds are listed below. They change depending on how far away the rider is from the starting line (e.g. max opening time at 800km would be calculated: 200/34 + (400-200)/32 + (600-400)/30 + (800-600)/28)

Distance from start | Minimum Speed | Maximum Speed
             0-60km      20km/h          34km/h
           60-200km      15km/h          34km/h
          200-400km      15km/h          32km/h
          400-600km      15km/h          30km/h
         600-1000km      11.428km/h      28km/h
            +1000km      13.333km/h      26km/h

Notes: The brevet distance can be closer to the start than the final controle. The final controle is calculated as stopping at whatever the brevet distance is. 


## Button Functionality
The display button will show the controle data stored in the database.
The submit button should clear the mongo database and update it with the currently filled fields. It will submit nothing and warn the user if:

	The brevet doesn't start at 0.
	A controle contains invalid data (e.g. "five" will fail, "10" or "10km" will succeed).
	The final controle distance is less than the brevet distance.
	The final controle is distance is over 20% longer than the brevet distance.
	The are gaps in the form.
	Nothing has been entered into the form.
	The controles aren't in ascending order.
	
This is all checked by javascript in calc.html

## Developer Notes
Made with HTML and javascript, flask, python, and mongodb for the database.

## Authors

Initial code by Michal Young. Revised by Adam Christensen

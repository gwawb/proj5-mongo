"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    print("IN CALC TIMES")
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    start = request.args.get('start', type=str)
    dist = request.args.get('dist', type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, dist, start)
    close_time = acp_times.close_time(km, dist, start)

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route("/display")
def display():
    _items = db.tododb.find()
    items = [item for item in _items]

    return flask.render_template('display.html', items=items)

@app.route("/enter", methods=['POST'])
def enter():
    db.tododb.remove({})
    km = request.form.getlist('km')
    name = request.form.getlist('location')
    open = request.form.getlist('open')
    close = request.form.getlist('close')
    item_doc = {
        'km': "km",
        'name': "name",
        'open': "open time",
        'close': "close time"
    }
    db.tododb.insert_one(item_doc)
    for x in range(20):
        if km[x] == '':
            return flask.render_template('calc.html')
        item_doc = {
            'km': km[x],
            'name': name[x],
            'open': open[x],
            'close': close[x]
        }
        db.tododb.insert_one(item_doc)

    return redirect(url_for('index'))

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
